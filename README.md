# laravel/homestead for Microsoft PHP Drivers for SQL Server 


## Cloning the repo

```
$ git clone https://bitbucket.org/f3s/homestead-mssql.git
```

## Avoid wasting your time

1. If you are using SQL Server instance, make sure your instance is *NOT* set to *dynamic* port. SQL Server Configuration Manager -> Your instance -> TCP/IP
2. If you are using SQL Server instance, you should define the connection as "server,port" with a comma, [very important](http://php.net/manual/en/ref.pdo-dblib.php#66917).
3. Make sure Windows firewall (host) allow inbound connection to that port (you may need to add a new rule)

## Starting the vagrant box

Edit `/vars.php` and change its values with your specific SQL Server parameters.

Load laravel/homestead files and create your vagrant file with those commands

```
composer require laravel/homestead --dev
vendor\\bin\\homestead make
```

Change that line in your `Vagrantfile` :

```
-- config.vm.provision "shell", path: afterScriptPath, privileged: false
++ config.vm.provision "shell", path: afterScriptPath, privileged: true
```

Setup your `Homestead.yaml` at will and start your box

```
vagrant up
```

If everything worked as expected, you should be able to enjoy a fresh laravel/homestead box that is compatible with Microsoft PHP Drivers for SQL Server


## Links

* [Laravel Homestead - Per Project Installation](https://laravel.com/docs/5.4/homestead#per-project-installation)
* [Microsoft PHP Drivers for SQL Server – Linux Install Instructions](https://www.microsoft.com/en-us/download/details.aspx?id=20098)