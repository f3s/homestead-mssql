sudo su
apt-get update

# Based on Microsoft guide to install Microsoft PHP Drivers for SQL Server on linux
# https://www.microsoft.com/en-us/download/details.aspx?id=20098

# Step 1: Install PHP (unless already installed)
# Nothing to do here

# Step 2: Install pre-requisites
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

apt-get update
sudo ACCEPT_EULA=Y apt-get install -y msodbcsql mssql-tools
apt-get install -y unixodbc-dev

# Step 3: Install Apache
# Nothing to do here

# Step 4: Install the Microsoft PHP Drivers for SQL Server
sudo pecl install sqlsrv
sudo pecl install pdo_sqlsrv

# Step 5: Load the Microsoft PHP Drivers for SQL Server
echo "extension=/usr/lib/php/20160303/sqlsrv.so" >> /etc/php/7.1/fpm/php.ini
echo "extension=/usr/lib/php/20160303/pdo_sqlsrv.so" >> /etc/php/7.1/fpm/php.ini
echo "extension=/usr/lib/php/20160303/sqlsrv.so" >> /etc/php/7.1/cli/php.ini
echo "extension=/usr/lib/php/20160303/pdo_sqlsrv.so" >> /etc/php/7.1/cli/php.ini

# Step 6: Restart Apache/nginx to load the new php.ini file
service nginx restart
service php7.1-fpm restart
